package com.example.demo_tmp;

import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.Assertions;
import org.json.JSONException;
import org.junit.jupiter.api.*;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import java.util.Collections;

@Log4j2
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DemoTmpApplicationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Order(1)
    @Test
    @DisplayName("It should return 404 when not found")
    void itShouldReturn404WhenNotFound() throws JSONException {
        // Given
        String url = "/not_found";
        String expected_404 = "{status:404}";
        // When
        String response = this.restTemplate.getForObject(url, String.class);
        // Then
        JSONAssert.assertEquals(expected_404, response, false);
    }

    @Order(2)
    @Test
    @DisplayName("It should return 201 when created")
    void itShouldReturn201WhenCreated() throws JSONException {
        // Given
        String url = "/api/v1/foo/";
        String requestBody = "{\"id\":1,\"field\":\"foo\"}";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> request = new HttpEntity<>(requestBody, headers);
        // When
        ResponseEntity<String> response = this.restTemplate.postForEntity(url, request, String.class);
        log.info(response.getBody());
        // Then
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        JSONAssert.assertEquals(requestBody, response.getBody(), false);
    }

    @Order(3)
    @Test
    @DisplayName("It should return 200 when update")
    void itShouldReturn200WhenUpdate() throws JSONException {
        // Given
        String url = "/api/v1/foo/1";
        String requestBody = "{\"id\":1,\"field\":\"foo\"}";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        HttpEntity<String> entity = new HttpEntity<>(requestBody, headers);
        // When
        ResponseEntity<String> response = this.restTemplate.exchange(url, HttpMethod.PUT, entity, String.class);
        log.info(response.getBody());
        // Then
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        JSONAssert.assertEquals(requestBody, response.getBody(), false);
    }
}

package com.example.demo_tmp.repositories;

import com.example.demo_tmp.entities.NewEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NewEntityRepository extends JpaRepository<NewEntity, Long> {
}
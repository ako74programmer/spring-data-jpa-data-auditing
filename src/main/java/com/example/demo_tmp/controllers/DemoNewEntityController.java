package com.example.demo_tmp.controllers;

import com.example.demo_tmp.entities.NewEntity;
import com.example.demo_tmp.entities.NewEntityDto;
import com.example.demo_tmp.repositories.NewEntityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/v1/foo")
public class DemoNewEntityController {

    private final NewEntityRepository repository;

    @PostMapping(path = "/", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<NewEntity> create(@Valid @RequestBody NewEntityDto body) {
        NewEntity newEntity = new NewEntity(null, body.field());

        return ResponseEntity.status(HttpStatus.CREATED).body(repository.save(newEntity));
    }


    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<NewEntity> update(@PathVariable Long id, @Valid @RequestBody NewEntityDto body) {

        NewEntity newEntity = new NewEntity(id, body.field());

        return ResponseEntity.ok().body(repository.save(newEntity));
    }
}

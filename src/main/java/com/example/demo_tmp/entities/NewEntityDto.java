package com.example.demo_tmp.entities;

import java.io.Serializable;
import java.util.Date;

public record NewEntityDto(String createdBy, Date createdDate, String lastModifiedBy, Date lastModifiedDate,
                           String field) implements Serializable {
}

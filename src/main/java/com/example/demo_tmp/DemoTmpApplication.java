package com.example.demo_tmp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoTmpApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoTmpApplication.class, args);
    }

}
